import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../shared/models/user.model';
import { UserService } from '../shared/services/user.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  columnsToDisplay = ['name', 'email', 'occupation', 'detailsAction', 'updateaAction', 'deleteAction'];
  users : User [] =[];
  isLoading = false;
  subscriptions: Subscription[] = [];

  constructor(private userService: UserService) { }
  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.userService.getUsers();

    let subscription = this.userService.users.subscribe(
      (users) => (this.users = users)
    );
    this.subscriptions.push(subscription);

    subscription = this.userService.isLoading.subscribe(
      (isLoading) => (this.isLoading = isLoading)
    );
    this.subscriptions.push(subscription);
  }

  onAssignementClick(user: User) {
    console.log(user);
    this.userService.selectedUser.next(user);
  }
}
