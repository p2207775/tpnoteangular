import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Observable, of, BehaviorSubject, catchError, map } from 'rxjs';
import { Router } from '@angular/router';
import { HttpService } from './http.service';

@Injectable({ "providedIn": 'root' })
export class UserService {
  users = new BehaviorSubject<User[]>([]);

  selectedUser = new BehaviorSubject<User | null>(null);

  constructor(private router: Router, private http: HttpService) { }



  isLoading = new BehaviorSubject<boolean>(false);

  getUsers(): void {
    this.isLoading.next(true);
    this.http
      .getUsers()
      .pipe(
        map((data: User[]) => {
          return data.map((element) => {
            element.id = element.id;
            element.name = element.name;
            element.occupation = element.occupation;
            element.email = element.email;
            element.bio = element.bio;
            return element;
          });
        }),
        catchError((error) => {
          this.isLoading.next(false);
          throw new Error(error);
        })
      )
      .subscribe((data: User[]) => {
        console.log(data);
        this.users.next(data);
        this.isLoading.next(false);
      });
  }



  selectUser(id: number) {
    this.isLoading.next(true);
    const sub = this.http
      .getUser(id)
      .subscribe((data) => {
        this.isLoading.next(false);
        this.selectedUser.next(data);
        sub.unsubscribe();
      });
  }

  addUser(User: User): void {
    const sub = this.http.addUser(User).subscribe((data) => {
      console.log(data);
      console.log('add Users');
      this.router.navigate(['home']);
      sub.unsubscribe;
    });
  }

  updateUser() {
    const selectedUser = this.selectedUser.getValue();
    if (selectedUser) {
      console.log('update one User');
      this.editUser();
    }
  }

  editUser() {
    const selectedUser = this.selectedUser.getValue();
    if (selectedUser) {
      const sub = this.http
        .putUser(selectedUser.id.toString(), selectedUser)
        .subscribe(() => {
          console.log('edit one User');
          this.router.navigate(['/home']);
          this.selectedUser.next(null);
          sub.unsubscribe();
        });
    }
  }

  deleteUser() {
    const selectedUser = this.selectedUser.getValue();
    if (selectedUser) {
      const sub = this.http
        .deleteUser(selectedUser.id.toString())
        .subscribe(() => {
          console.log('delete one User');
          this.selectedUser.next(null);
          this.router.navigate(['/home']);
          sub.unsubscribe();
        });
    }
  }


}