import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private httpClient: HttpClient) { }

  baseUrl = 'https://65844d944d1ee97c6bcf7285.mockapi.io/users';

  public getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseUrl);
  }

  public getUser(id: number): Observable<User> {
    return this.httpClient.get<User>(`${this.baseUrl}/${id}`);
  }

  public addUser(User: User): Observable<User> {
    return this.httpClient.post<User>(`${this.baseUrl}`, User);
  }

  public deleteUser(id: string): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`, {
      responseType: 'text',
    });
  }

  public putUser(id: string, User: User): Observable<any> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, User);
  }
}
