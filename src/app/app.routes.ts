import { Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserDeleteComponent } from './user-delete/user-delete.component';
export const routes: Routes = [
{ path: '', redirectTo: 'home', pathMatch: 'full' },
{ path: 'home', component: UsersComponent },
{path : 'user/:id', component : UserDetailComponent},
{path: 'user/:id/edit', component : UserEditComponent},
{path : 'user/:id/delete', component :UserDeleteComponent},
];